/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Institute;
import java.util.*;
/**
 *
 * @author Matthew
 */
public class Library {
    private String name;
    private ArrayList<Book> group = new ArrayList<Book>();
    
    public Library(String name, ArrayList<Book> group){
        this.name = name;
        this.group = group;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setGroup(ArrayList<Book> group){
        this.group = group;
    }
    
    public ArrayList<Book> getGroup(){
        return group;
    }
}
