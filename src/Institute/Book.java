/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Institute;

/**
 *
 * @author Matthew
 */
public class Book {
    private String title;
    private String author;
    
    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public String getTitlte(){
        return title;
    }
    
    public void setAuthor(String author){
        this.author = author;
    }
    
    public String getAuthor(){
        return author;
    }
    
    public String toString(){
        return "title: " + title + " Author: " + author;
    }
}
